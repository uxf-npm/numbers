# @uxf/numbers
[![npm](https://img.shields.io/npm/v/@uxf/numbers)](https://www.npmjs.com/package/@uxf/numbers)
[![size](https://img.shields.io/bundlephobia/min/@uxf/numbers)](https://www.npmjs.com/package/@uxf/numbers)
[![quality](https://img.shields.io/npms-io/quality-score/@uxf/numbers)](https://www.npmjs.com/package/@uxf/numbers)
[![license](https://img.shields.io/npm/l/@uxf/numbers)](https://www.npmjs.com/package/@uxf/numbers)
## Price formatter

### formatPrice(value, options)

Allows price formatting to different currencies (default is CZK).

Options:

| name          | meaning                                                                    | default    |
| ------------- | -------------------------------------------------------------------------- |:---------- |
| currency      | currency                                                                   | "CZK"      |
| decimals      | count of decimals                                                          | 0          |
| displaySign   | should sign be displayed                                                   | "negative" |
| formatOptions | custom options from [currency.js](https://github.com/scurker/currency.js/) | {}         |
| hideSymbol    | hides currency symbol                                                      | false      |

## Phone number formatter

### formatPhoneNumber(value, prefix)

Allows phone number formatting.

## Number formatter

### formatNumber(value, options)

Allows simple number formatting.

Options: [currency.js](https://github.com/scurker/currency.js/)
