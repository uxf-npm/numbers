export const createElement = (text: string, tag = "div") => {
    const el = document.createElement(tag);
    el.innerText = text;

    return el;
};
