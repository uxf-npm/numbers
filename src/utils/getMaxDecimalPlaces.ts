import currencyjs, { Any } from "currency.js";

export const getMaxDecimalPlaces = (value: Any, precision: number, isFixedDecimal: boolean) => {
    if (isFixedDecimal) {
        return Math.min(
            currencyjs(value, { precision: 9 }).value.toFixed(precision).split(".")[1]?.length || 0,
            precision,
        );
    }

    return Math.min(currencyjs(value, { precision: 9 }).value.toString().split(".")[1]?.length || 0, precision);
};
