import { getMaxDecimalPlaces } from "./getMaxDecimalPlaces";

test("test getMaxDecimalPlaces returns correct values", () => {
    expect(getMaxDecimalPlaces(0, 0, false)).toBe(0);
    expect(getMaxDecimalPlaces(5000, 0, false)).toBe(0);
    expect(getMaxDecimalPlaces(5000.123, 1, false)).toBe(1);
    expect(getMaxDecimalPlaces(5000.123, 3, false)).toBe(3);
    expect(getMaxDecimalPlaces(5000.123, 5, false)).toBe(3);
    expect(getMaxDecimalPlaces(5000.1, 1, false)).toBe(1);
    expect(getMaxDecimalPlaces(5000.12, 3, false)).toBe(2);
    expect(getMaxDecimalPlaces(5000.123, 5, false)).toBe(3);

    expect(getMaxDecimalPlaces(0, 0, true)).toBe(0);
    expect(getMaxDecimalPlaces(5000, 0, true)).toBe(0);
    expect(getMaxDecimalPlaces(5000, 3, true)).toBe(3);
    expect(getMaxDecimalPlaces(5000.123, 1, true)).toBe(1);
    expect(getMaxDecimalPlaces(5000.123, 3, true)).toBe(3);
    expect(getMaxDecimalPlaces(5000.123, 5, true)).toBe(5);
    expect(getMaxDecimalPlaces(5000.1, 1, true)).toBe(1);
    expect(getMaxDecimalPlaces(5000.12, 3, true)).toBe(3);
    expect(getMaxDecimalPlaces(5000.123, 5, true)).toBe(5);

    expect(getMaxDecimalPlaces("", 3, true)).toBe(3);
    expect(getMaxDecimalPlaces("0", 3, true)).toBe(3);
    expect(getMaxDecimalPlaces("5000", 3, true)).toBe(3);
    expect(getMaxDecimalPlaces("5000.123", 1, true)).toBe(1);
    expect(getMaxDecimalPlaces("5000.123", 3, true)).toBe(3);
    expect(getMaxDecimalPlaces("5000.123", 5, true)).toBe(5);
    expect(getMaxDecimalPlaces("5000.1", 1, true)).toBe(1);
    expect(getMaxDecimalPlaces("5000.12", 3, true)).toBe(3);
    expect(getMaxDecimalPlaces("5000.123", 5, true)).toBe(5);
});
