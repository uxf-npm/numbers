import currencyjs, { Any, Options } from "currency.js";

const DEFAULT_OPTIONS: Options = {
    decimal: ",",
    negativePattern: "-#",
    pattern: "#",
    separator: "\xa0",
};

export const formatNumber = (value: Any, options?: Options): string => {
    const { precision = 0, ...finalOptions } = { ...DEFAULT_OPTIONS, ...options };

    return currencyjs(value, { precision }).format(finalOptions);
};
