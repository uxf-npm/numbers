const TEST_REGEX = "(/tests/.*|(\\.|/)(test|spec))\\.(jsx?|js?|tsx?|ts?)$";

module.exports = {
    testRegex: TEST_REGEX,
    testPathIgnorePatterns: ["<rootDir>/node_modules/"],
    moduleFileExtensions: ["ts", "tsx", "js", "jsx"],
    transform: {
        "^.+\\.(ts|tsx|js|jsx)$": "ts-jest",
    },
    collectCoverageFrom: ["<rootDir>/src/**"],
    coverageReporters: ["text", "cobertura", "html"],
};
