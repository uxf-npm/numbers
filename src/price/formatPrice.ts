import currencyjs, { Any, Options } from "currency.js";
import { getMaxDecimalPlaces } from "../utils/getMaxDecimalPlaces";

export type Currency = "CZK" | "EUR";
export type DisplaySign = "negative" | "always" | "exceptZero";

export interface FormatPriceOptions {
    abbreviateBigNumber?: boolean;
    currency?: Currency;
    decimals?: number;
    displaySign?: DisplaySign;
    formatOptions?: Options;
    hideSymbol?: boolean;
    isFixedDecimal?: boolean;
}

const VALUE_ABBREVIATIONS = [
    { value: 1, symbol: "" },
    { value: 1e3, symbol: "tis." },
    { value: 1e6, symbol: "mil." },
    { value: 1e9, symbol: "mld." },
    { value: 1e12, symbol: "bil." },
];

const CURRENCY_OPTIONS: Record<Currency, Options> = {
    CZK: { symbol: "\xa0Kč", decimal: ",", separator: "\xa0", pattern: "#!", negativePattern: "#!" },
    EUR: { symbol: "€", decimal: ",", separator: ".", pattern: "!#", negativePattern: "!#" },
};

const NON_ZERO_DISPLAY_SIGN: DisplaySign[] = ["always", "exceptZero"];

export const formatPrice = (value: Any, options?: FormatPriceOptions): string => {
    const {
        abbreviateBigNumber = false,
        currency = "CZK",
        decimals = 0,
        displaySign = "negative",
        formatOptions = {},
        hideSymbol = false,
        isFixedDecimal = false,
    } = options || {};

    const finalOptions: Options = { ...CURRENCY_OPTIONS[currency], ...formatOptions };

    if (
        (Number(value) > 0 && NON_ZERO_DISPLAY_SIGN.includes(displaySign)) ||
        (Number(value) === 0 && displaySign === "always")
    ) {
        finalOptions.pattern = `+${finalOptions.pattern}`;
    } else if (Number(value) < 0 && (NON_ZERO_DISPLAY_SIGN.includes(displaySign) || displaySign === "negative")) {
        finalOptions.negativePattern = `\u2212${finalOptions.negativePattern}`;
    }

    if (hideSymbol) {
        finalOptions.pattern = finalOptions.pattern?.replace("!", "");
        finalOptions.negativePattern = finalOptions.negativePattern?.replace("!", "");
    }

    if (abbreviateBigNumber) {
        const abbreviation = VALUE_ABBREVIATIONS.slice()
            .reverse()
            .find(function (i) {
                return value >= i.value;
            });

        if (abbreviation) {
            finalOptions.symbol = `\xa0${abbreviation.symbol}${finalOptions.symbol}`;
            return currencyjs(Number(value) / abbreviation.value, {
                precision: getMaxDecimalPlaces(Number(value) / abbreviation.value, decimals, isFixedDecimal),
            }).format(finalOptions);
        }
    }

    return currencyjs(value, { precision: getMaxDecimalPlaces(value, decimals, isFixedDecimal) }).format(finalOptions);
};
