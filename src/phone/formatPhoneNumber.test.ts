import { formatPhoneNumber } from "./formatPhoneNumber";

test("fotmat phone number", () => {
    expect(formatPhoneNumber(123456789)).toBe(`123\xa0456\xa0789`);
    expect(formatPhoneNumber("+420123456789")).toBe(`+420\xa0123\xa0456\xa0789`);
    expect(formatPhoneNumber(123456789, "+420")).toBe(`+420\xa0123\xa0456\xa0789`);
});
