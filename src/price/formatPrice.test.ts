import { formatPrice } from "./formatPrice";

test("format price in czech format", () => {
    expect(formatPrice(5000.12)).toBe(`5\xa0000\xa0Kč`);
    expect(formatPrice(5000.12, { hideSymbol: true })).toBe(`5\xa0000`);
    expect(formatPrice(5000.12345, { decimals: 2 })).toBe(`5\xa0000,12\xa0Kč`);
    expect(
        formatPrice(5000.15, {
            decimals: 3,
            isFixedDecimal: true,
        }),
    ).toBe(`5\xa0000,150\xa0Kč`);
    expect(formatPrice(-5000, { displaySign: "always" })).toBe(`\u22125\xa0000\xa0Kč`);
    expect(
        formatPrice(0, {
            displaySign: "always",
        }),
    ).toBe(`+0\xa0Kč`);
    expect(formatPrice(5000, { displaySign: "always" })).toBe(`+5\xa0000\xa0Kč`);
    expect(formatPrice(5000.12, { displaySign: "negative" })).toBe(`5\xa0000\xa0Kč`);
    expect(formatPrice(0, { displaySign: "negative" })).toBe(`0\xa0Kč`);
    expect(formatPrice(-5000.12, { displaySign: "negative" })).toBe("\u22125\xa0000\xa0Kč");
    expect(
        formatPrice(5000.12, {
            displaySign: "exceptZero",
        }),
    ).toBe(`+5\xa0000\xa0Kč`);
    expect(
        formatPrice(0, {
            displaySign: "exceptZero",
        }),
    ).toBe(`0\xa0Kč`);
    expect(
        formatPrice(-5000.12, {
            displaySign: "exceptZero",
        }),
    ).toBe(`\u22125\xa0000\xa0Kč`);
});

test("format price in EUR format", () => {
    expect(formatPrice(5000.12, { currency: "EUR" })).toBe("€5.000");
    expect(formatPrice(5000.12, { currency: "EUR", hideSymbol: true })).toBe("5.000");
});

test("format price with big number abbreviation", () => {
    expect(formatPrice(153000000, { abbreviateBigNumber: true })).toBe("153\xa0mil.\xa0Kč");
    expect(formatPrice(253300000, { abbreviateBigNumber: true, decimals: 2 })).toBe("253,3\xa0mil.\xa0Kč");
    expect(formatPrice(2500000, { abbreviateBigNumber: true, decimals: 1 })).toBe("2,5\xa0mil.\xa0Kč");
    expect(formatPrice(15000000, { abbreviateBigNumber: true })).toBe("15\xa0mil.\xa0Kč");
    expect(formatPrice(155000, { abbreviateBigNumber: true })).toBe("155\xa0tis.\xa0Kč");
});
