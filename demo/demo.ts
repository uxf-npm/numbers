import { formatNumber } from "../src/number/formatNumber";
import { formatPhoneNumber } from "../src/phone/formatPhoneNumber";
import { formatPrice } from "../src/price/formatPrice";
import { createElement } from "./utils";

const app = document.getElementById("app");

// czech price
app.appendChild(createElement(`Czech price: ${formatPrice(5000.12)}`));
// czech price without symbol
app.appendChild(createElement(`Czech price without symbol: ${formatPrice(5000.12, { hideSymbol: true })}`));
// czech price with two decimals
app.appendChild(createElement(`Czech price with two decimals: ${formatPrice(5000.12345, { decimals: 2 })}`));
// czech price with fixed decimals part
app.appendChild(
    createElement(
        `Czech price with fixed decimals part: ${formatPrice(5000.15, {
            decimals: 3,
            isFixedDecimal: true,
        })}`,
    ),
);
// eur price
app.appendChild(createElement(`EUR price: ${formatPrice(5000.12, { currency: "EUR" })}`));
// eur price without symbol
app.appendChild(
    createElement(`EUR price without symbol: ${formatPrice(5000.12, { currency: "EUR", hideSymbol: true })}`),
);
// czech price sign
app.appendChild(
    createElement(
        `Czech price with sign always: ${formatPrice(-5000, { displaySign: "always" })},  ${formatPrice(0, {
            displaySign: "always",
        })},  ${formatPrice(5000, { displaySign: "always" })}`,
    ),
);
// czech price sign
app.appendChild(
    createElement(
        `Czech price with sign for negative only: ${formatPrice(5000.12)}, ${formatPrice(0)}, ${formatPrice(-5000.12)}`,
    ),
);
// czech price sign
app.appendChild(
    createElement(
        `Czech price with sign except zero: ${formatPrice(5000.12, {
            displaySign: "exceptZero",
        })}, ${formatPrice(0, {
            displaySign: "exceptZero",
        })}, ${formatPrice(-5000.12, {
            displaySign: "exceptZero",
        })}`,
    ),
);

// phone number
app.appendChild(createElement(`Phone number: ${formatPhoneNumber(123456789)}`));
// phone number
app.appendChild(createElement(`Phone number: ${formatPhoneNumber("+420123456789")}`));
// phone number
app.appendChild(createElement(`Phone number: ${formatPhoneNumber(123456789, "+420")}`));

// number
app.appendChild(createElement(`Number: ${formatNumber(123456)}`));
// number
app.appendChild(createElement(`Number: ${formatNumber(123456.98798, { decimal: ",", separator: "." })}`));

// prices with big number abbreviation
app.appendChild(
    createElement(`Price with big number abbreviation: ${formatPrice(153000000, { abbreviateBigNumber: true })}`),
);
app.appendChild(
    createElement(
        `Price with big number abbreviation: ${formatPrice(253300000, { abbreviateBigNumber: true, decimals: 2 })}`,
    ),
);
app.appendChild(
    createElement(
        `Price with big number abbreviation: ${formatPrice(2500000, { abbreviateBigNumber: true, decimals: 1 })}`,
    ),
);
app.appendChild(
    createElement(`Price with big number abbreviation: ${formatPrice(15000000, { abbreviateBigNumber: true })}`),
);
app.appendChild(
    createElement(`Price with big number abbreviation: ${formatPrice(155000, { abbreviateBigNumber: true })}`),
);
