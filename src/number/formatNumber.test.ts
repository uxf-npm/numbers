import { formatNumber } from "./formatNumber";

test("format number", () => {
    expect(formatNumber(123)).toBe(`123`);
    expect(formatNumber(123456)).toBe(`123\xa0456`);
    expect(formatNumber(123456.98798)).toBe(`123\xa0457`);
    expect(formatNumber(123456789)).toBe(`123\xa0456\xa0789`);
    expect(formatNumber(123.456789, { precision: 3 })).toBe(`123,457`);
    expect(formatNumber(123456.98798, { decimal: ",", separator: "." })).toBe(`123.457`);
    expect(formatNumber(123456.9123, { decimal: ",", separator: ".", precision: 1 })).toBe(`123.456,9`);
});
