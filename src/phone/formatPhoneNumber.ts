import currencyjs, { Any } from "currency.js";

export const formatPhoneNumber = (value: Any, prefix?: string): string => {
    const prefixText = typeof value === "string" && value.startsWith("+") ? "+" : prefix ? `${prefix}\xa0` : "";

    return `${prefixText}${currencyjs(value, { precision: 0 }).format({ separator: "\xa0", pattern: "#" })}`;
};
